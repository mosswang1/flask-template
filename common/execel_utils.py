'''
传入表头和数据行，生成excel
'''


import os,sys,time,logging
from datetime import date, timedelta
logging.basicConfig(level=logging.INFO)
import datetime
import zipfile
import pathlib

import pymysql
import xlrd,xlwt
import xlsxwriter

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]

print(rootPath)
sys.path.append(rootPath)

excel_path = "/opt/output/data/2023-03-16/"


'''
写入xlsx文件
'''
def write_data_to_excels(first_data, data_list, excel_path):
    # 创建工作簿
    workbook = xlsxwriter.Workbook(excel_path)
    worksheet1 = workbook.add_worksheet('数据')  # 括号内为工作表表名

    bold = workbook.add_format({
        'bold':  True,  # 字体加粗
        'border': 1,  # 单元格边框宽度
        'align': 'left',  # 水平对齐方式
        'valign': 'vcenter',  # 垂直对齐方式
        #'fg_color': '#F4B084',  # 单元格背景颜色
        'text_wrap': True,  # 是否自动换行
    })

    # 添加表头
    if first_data != None and len(first_data) > 0:
        for index, value in enumerate(first_data):
            worksheet1.set_column(0, index, 15)
            worksheet1.write(0, index, value, bold)

    for row_index, row_data in enumerate(data_list):
        for cell_index, cell_value in enumerate(row_data):
            #print(type(cell_value))
            #worksheet1.set_column(row_index+1, cell_index, 25)
            if isinstance(cell_value, datetime.datetime):
                worksheet1.write(row_index+1, cell_index, cell_value.strftime("%Y-%m-%d %H:%M:%S"), bold)
            else:
                worksheet1.write(row_index+1, cell_index, str(cell_value), bold)

    workbook.close()
    print(excel_path)
    return excel_path

def execute_logic(header_data , data_list, output_path=None):
    return write_data_to_excels(header_data, data_list, output_path)

def gen_excel(header_data : list, data_list : list, output_path=None, file_name=None):
    if output_path == None:
        output_path = excel_path
    if file_name == None:
        file_name = '数据导出'
    os.makedirs(output_path, exist_ok=True)

    output_path = os.path.join(output_path, file_name)
    path = execute_logic(header_data, data_list, output_path)
    return path

if __name__ == '__main__':
    pass