# -*- encoding: utf-8 -*-
'''
@文件        :bi_wh_data.py
@说明        :
@时间        :2022-07-03 21:00:43
@作者        :wangxh
@版本        :1.0
'''

import os,sys,time
import pymysql
import datetime

from common.base_logger import logger as logging


table_map = [
    {
        "table_name":"wh_direct_sales_bill_detail",
        "file_name":"直销预付账单表T10",
        "query_key":"confirm_time"
    },
    {
        "table_name":"wh_bill_item_account_detail",
        "file_name":"商旅预付账单T10",
        "query_key":"confirm_time"
    },
    {
        "table_name":"wh_distribution_sales_bill_detail",
        "file_name":"分销预付账单T10",
        "query_key":"confirm_time"
    },
    {
        "table_name":"wh_bill_dis_detail",
        "file_name":"分销现付账单T10",
        "query_key":"create_time"
    },
    {
        "table_name":"wh_bill_gp_reservation",
        "file_name":"会籍卡账单",
        "query_key":"createtime"
    },
    {
        "table_name":"wh_offline_pay_book_keeping_account_bill",
        "file_name":"线下支付记账科目账单",
        "query_key":"create_time"
    },
    {
        "table_name":"wh_offline_pay_book_keeping_account_bill_ext",
        "file_name":"线下支付记账科目账单额外",
        "query_key":"create_time"
    },
    {
        "table_name":"wh_bill_dsl_reserv_detail",
        "file_name":"直销服务费账单",
        "query_key":"createtime"
    },
    {
        "table_name":"wh_system_service_cost_bill",
        "file_name":"系统服务费账单",
        "query_key":"create_time"
    }
]

prod_config = {
     "host":"10.88.5.153",
     "port":3306,
     "user":"root",
     "password":"g91xlIa&YC0Y",
     "database":"bi-wh-data",
}

config={
   "host":"121.37.162.220",
   "port":3306,
   "user":"root",
   "password":"Aa123456",
   "database":"bi-wh-data",
}

# config={
#    "host":"10.99.3.14",
#    "port":3306,
#    "user":"root",
#    "password":"Aa123456",
#    "database":"bi-wh-data",
# }

# 获取数据库连接
def get_db_conn(env=None):
    if env == "prod":
        return pymysql.connect(**prod_config)
    return pymysql.connect(**config)

# 获取表的所有字段名和字段注释
def get_table_columnames(table_name, conn):
    session = None
    cursor = None
    res_data = None

    try:
        #session = get_db_conn()
        #cursor = session.cursor()
        cursor = conn.cursor(pymysql.cursors.DictCursor)  
        # with pymysql.connect(db='foo', cursorclass=DictCursor) as cursor:
        #   
        sql = "SELECT column_name,column_comment FROM information_schema.COLUMNS WHERE table_schema='{}' AND table_name='{}'".format(config.get('database'), table_name)
        logging.info("execute sql: {}".format(sql))

        cursor.execute(sql)
        res_data= cursor.fetchall()
        #print(res_data)
    except Exception as e:
        logging.info("执行数据库操作异常: {}", e)
        conn.close()
    finally:
        #session.close()
        cursor.close()

    return res_data

# 执行sql查询表数据
def get_db_data(sql, conn):
    session = None
    cursor = None
    res_data = None

    try:
        #session = get_db_conn()
        cursor = conn.cursor()
        #cursor = conn.cursor(pymysql.cursors.DictCursor)
        logging.info("execute sql: {}".format(sql))

        cursor.execute(sql)
        res_data = cursor.fetchall()
        #print(res_data)
    except Exception as e:
        logging.info("执行数据库操作异常: {}", e)
        conn.close()
    finally:
        #session.close()
        cursor.close()
    
    return res_data


# 执行sql查询表数据
def get_db_datas(sql, conn):
    session = None
    cursor = None
    res_data = None

    try:
        # session = get_db_conn()
        #cursor = conn.cursor()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        logging.info("execute sql: {}".format(sql))

        cursor.execute(sql)
        res_data = cursor.fetchall()
        # print(res_data)
    except Exception as e:
        logging.info("执行数据库操作异常: {}", e)
        conn.close()
    finally:
        # session.close()
        cursor.close()

    return res_data

# 保存或更新sql
def save_or_update(sql, conn):
    cursor = None
    res_data = None

    try:
        cursor = conn.cursor()
        logging.info("execute sql: {}".format(sql))

        cursor.execute(sql)
        res_data = cursor.fetchall()
        conn.commit()
    except Exception as e:
        logging.info("执行数据库操作异常: {}", e)
    finally:
        cursor.close()

    return res_data


'''
批量保存
param_list: [('a',1),('b',2),(None,3)]  # 元素是不是元组都可以
'''
def batch_save(sql, param_list, conn):
    cursor = None

    try:
        cursor = conn.cursor()
        cursor.executemany(sql, param_list)
        logging.info(f'sql: {sql}, \nparams:{param_list}')
        conn.commit()
    except Exception as e:
        logging.info("执行数据库操作异常: {}", e)
    finally:
        cursor.close()

    pass

# 获取表的所有字段名和字段注释
def get_table_columnames(table_name, conn):
    session = None
    cursor = None
    res_data = None

    try:
        #session = get_db_conn()
        #cursor = session.cursor()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        #cursor = conn.cursor()
        # with pymysql.connect(db='foo', cursorclass=DictCursor) as cursor:
        #
        sql = "SELECT column_name,column_comment FROM information_schema.COLUMNS WHERE table_schema='{}' AND table_name='{}'".format('bi-wh-data', table_name)
        print("execute sql: {}".format(sql))

        cursor.execute(sql)
        res_data= cursor.fetchall()
        #print(res_data)
    except Exception as e:
        logging.info("执行数据库操作异常: {}", e)
        conn.close()
    finally:
        #session.close()
        cursor.close()

    return res_data

