import os
import sys

cur_path = os.path.abspath(os.path.dirname(__file__))
# 项目根目录 D:\python_workspace\wangxh-tools
root_path = os.path.split(cur_path)[0]
sys.path.append(root_path)
# 项目根目录的父级目录 D:\python_workspace
root_parent_path = os.path.dirname(root_path)
# 应用名称
app_name = 'flask-template'

# 环境标识 默认dev
env = None
if len(sys.argv) > 1:
    env = sys.argv[1]
else:
    env = 'dev'

if __name__ == '__main__':
    print(sys.path)