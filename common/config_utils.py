import os
import sys
import yaml

from common.constants import root_path
from common.constants import root_parent_path

# key以.分隔 exp:
def get_value(key=None, env=None):
    if env == None:
        env = 'dev'
    if key == None:
        return None
    yaml_file_name = f'application-{env}.yml'

    key_list = key.split('.')
    tmp_data = None
    with open(os.path.join(root_path, 'conf', yaml_file_name), encoding="UTF-8") as fs:
        datas = yaml.load(fs, Loader=yaml.FullLoader)
        for item_key in key_list:
            if tmp_data == None:
                tmp_data = datas.get(item_key)
            else:
                tmp_data = tmp_data.get(item_key)

        return tmp_data


if __name__ == '__main__':
    # fs = open(os.path.join(root_path, 'conf', "application-dev.yml"), encoding="UTF-8")
    # datas = yaml.load(fs, Loader=yaml.FullLoader)  # 添加后就不警告了
    # print(datas)
    value = get_value('db.bi-wh-data.ip')
    print(value)