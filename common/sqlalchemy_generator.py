# -*- encoding: utf-8 -*-
'''
@文件        :sqlalchemy_generator.py
@说明        :
@时间        :2021-11-25 15:21:59
@作者        :wangxh
@版本        :1.0
'''

import os,sys,time,logging
logging.basicConfig(level=logging.INFO)

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from urllib.parse import quote_plus as urlquote

def gen_mysql():
    #db_url = config_utils.get_value("mysql_bi_wh_data", "db.url")
    db_passw = 'g91xlIa&YC0Y'
    db_url = 'mysql+pymysql://root:{}@10.88.5.153:3306/bi-wh-data'.format(urlquote(db_passw))
    tablle_name = "sm_pull_conf"
    entity_name = os.path.join(rootPath, "db\\enity\\sm_pull_conf.py")

    sql_cmd = "sqlacodegen --tables {} {} > {}".format(tablle_name, db_url, entity_name)

    print(sql_cmd)
    os.system(sql_cmd)

def gen_sqlite():
    db_url = "sqlite:///" + os.path.join(rootPath, "db\\flask-template-dev.db")
    tablle_name = "sys_config"
    entity_name = os.path.join(rootPath, "db\\enity\\sys_config.py")

    sql_cmd = "sqlacodegen --tables {} {} >{}".format(tablle_name, db_url, entity_name)

    print(sql_cmd)
    os.system(sql_cmd)


if __name__=="__main__":
    
    # db_url = config_utils.get_value("mysql_bi_wh_data", "db.url")
    # tablle_name = "sm_pull_conf"
    # entity_name = os.path.join(os.getcwd(), "db\\entity\\bi_wh_pull_conf_entity.py")
    
    # db_url = config_utils.get_value("mysql_joint_wisdom_data", "db.url")
    # tablle_name = "sm_pull_conf"
    # entity_name = os.path.join(os.getcwd(), "db\\entity\\joint_wisdom_pull_conf_entity.py")
    
    #gen_mysql()
    gen_sqlite()


    pass
