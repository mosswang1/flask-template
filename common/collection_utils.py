from common.base_logger import logger as logging

source_one_txt = r"E:\\test1\\1.txt"
source_two_txt = r"E:\\test1\\2.txt"


# 计算两个list的交集 并集 差集
def test1():
    a = (1, 2 ,4, 7, 8)
    b = (1, 7, 8, 9)

    # 交集 同时存在于A 与 B 中的元素
    #c = set(a) & set(b)
    # 并集 A 和 B 中的所有元素
    #c = set(a) | set(b)
    # 差集 只存在于 A 中的元素/B中的元素
    c = set(a) - set(b)
    d = set(b) - set(a)

    print(c)
    print(d)

    pass

def get_only_exits_list(a : list, b : list):
    res = set(a) - set(b)
    return res

if __name__ == '__main__':
    #test1()
    list_one = []

    list_two = []

    with open(source_one_txt, mode="r", encoding="utf-8") as file1:
        for item in file1.readlines():
            list_one.append(item[:-1])

    with open(source_two_txt, mode="r", encoding="utf-8") as file2:
        for item in file2.readlines():
            list_two.append(item[:-1])

    list_two = list(set(list_two))

    logging.info("listone: {}, listtwo: {}".format(len(list_one), len(list_two)))

    res = get_only_exits_list(list_one, list_two)
    logging.info(len(res))
    logging.info(res)

    pass
