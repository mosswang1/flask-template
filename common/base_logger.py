import os
import sys
import time
import logging

from common.constants import root_parent_path
from logging.handlers import TimedRotatingFileHandler


class Logger():
    # 2023-03-22 12:33:30 [com.executor.jobs.CouponsOrderDataPullJob#couponsOrderDataPullJobHandler]-[245]-[Thread-795] 多线程写入数据库, 总耗时376ms
    log_fmt = '%(asctime)s %(levelname)s [%(name)s] [%(thread)s-%(thread)d] %(filename)s %(funcName)s:%(lineno)d %(message)s'
    file_fmt = logging.Formatter(fmt=log_fmt)

    def __init__(self, level='INFO'):
        self.logger = logging.getLogger()
        self.logger.setLevel(level)

        self.get_log()

    def file_handler(self, level="INFO"):
        log_dir = os.path.join(root_parent_path, "logs/")
        today = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        hour = time.strftime('%H', time.localtime(time.time()))
        full_path = os.path.join(log_dir, today)
        if not os.path.exists(full_path):
            os.makedirs(full_path)
        log_path = os.path.join(full_path, f"apps.log.{today}-{hour}")

        #file_handler = logging.FileHandler(log_path, encoding="utf8")
        file_handler = TimedRotatingFileHandler(log_path, when='h', encoding='utf-8')
        file_handler.setLevel(level)
        file_handler.set_name('file_handler')
        file_handler.setFormatter(self.file_fmt)

        return file_handler

    def console_handler(self, level="INFO"):
        console_handler = logging.StreamHandler()
        console_handler.set_name('console_handler')
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(self.file_fmt)

        return console_handler

    def get_log(self):
        self.logger.addHandler(self.console_handler())
        self.logger.addHandler(self.file_handler())

        return self.logger

# 单例对象
logger = Logger().logger
#print(logger.handlers)
