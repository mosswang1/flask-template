import os
import zipfile
import pathlib



def gen_zip_file(file_path, file_name):
    os.makedirs(file_path, exist_ok=True)
    # excel文件目录
    directory = pathlib.Path(file_path)
    # 判断目录下是否有文件或文件夹
    if len(os.listdir(directory)) == 0:
        return None
    # zip文件目录
    zip_file_path = os.path.join(file_path, file_name)
    # 压缩算法 compression=zipfile.ZIP_LZMA
    with zipfile.ZipFile(zip_file_path, mode='w', compression=zipfile.ZIP_LZMA) as archive:
        for file_path in directory.iterdir():
            # 目录过滤掉，只打包文件
            if file_path.suffix != '' and file_path.suffix != '.zip':
                archive.write(file_path, arcname=file_path.name)

    return zip_file_path

if __name__ == '__main__':
    # start_day = '2023-03-21'
    # end_day = '2023-03-21'
    # excel_path = '/opt/output/data/2023-03-21/'
    # zip_file_name = f'{start_day}~{end_day}.zip'
    #gen_zip_file('/opt/output/data/2023-03-22/', '1.zip')

    # directory = pathlib.Path('/opt/output/data/2023-03-22/')
    # list = os.listdir(directory)
    # print(list)

    pass

