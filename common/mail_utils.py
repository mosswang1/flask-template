# -*- encoding: utf-8 -*-
'''
@文件        :mail_utils.py
@说明        :
@时间        :2021-12-08 09:58:15
@作者        :wangxh
@版本        :1.0
'''

import os,sys,time,logging
logging.basicConfig(level=logging.INFO)

import json
import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage


sys.path.append(os.getcwd())
from common import config_utils as config_util
from common import date_util as dateUtil


'''
发送普通文本邮件
发送html邮件 title 邮件标题 template_str 邮件内容 
mail_to 邮件接收者(列表) mail_from 邮件发送者
'''
def send_mail(mail_title, template_str, mail_to, mail_from, env):
    mail_host = config_util.get_value('mail_qq.host', env)  #设置服务器
    mail_port = config_util.get_value('mail_qq.port', env)
    mail_user = config_util.get_value('mail_qq.username', env)    #用户名
    mail_pass = config_util.get_value('mail_qq.password', env)   #口令
    
    sender = mail_user
    receivers = mail_to 
    message = MIMEText(template_str, 'plain', 'utf-8')
    message['From'] = Header(mail_from, 'utf-8')     # 发送者
    message['To'] =  Header(mail_to, 'utf-8')          # 接收者
    subject = '{}--{}'.format(mail_title, dateUtil.get_day(-1))
    message['Subject'] = Header(subject, 'utf-8')

    try:
        #smtpObj = smtplib.SMTP() 
        smtpObj = smtplib.SMTP_SSL(mail_host, mail_port)
        #smtpObj.connect(mail_host, mail_port)    # 25 为 SMTP 端口号
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(sender, receivers, message.as_string())
        print ("邮件发送成功")
    except smtplib.SMTPException:
        print ("Error: 无法发送邮件")

'''
发送html邮件 title 邮件标题 template_str htmnl邮件内容 
mail_to 邮件接收者(列表) mail_from 邮件发送者
'''
def send_html_mail(mail_title, template_str, mail_to, mail_from, env):
    mail_host = config_util.get_value('mail_qq.host', env)  # 设置服务器
    mail_port = config_util.get_value('mail_qq.port', env)
    mail_user = config_util.get_value('mail_qq.username', env)  # 用户名
    mail_pass = config_util.get_value('mail_qq.password', env)  # 口令
    
    sender = mail_user
    receivers = mail_to 
    message = MIMEText(template_str, 'html', 'utf-8')
    message['From'] = Header(mail_from, 'utf-8')     # 发送者
    message['To'] =  Header(mail_to, 'utf-8')          # 接收者
    subject = '{}--{}'.format(mail_title, dateUtil.get_day(-1))
    message['Subject'] = Header(subject, 'utf-8')

    try:
        #smtpObj = smtplib.SMTP() 
        smtpObj = smtplib.SMTP_SSL(mail_host, mail_port)
        #smtpObj.connect(mail_host, mail_port)    # 25 为 SMTP 端口号
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(sender, receivers, message.as_string())
        print ("邮件发送成功")
    except smtplib.SMTPException:
        print ("Error: 无法发送邮件")


'''
发送html邮件 title 邮件标题 template_str htmnl邮件内容 
mail_to 邮件接收者(列表) mail_from 邮件发送者
'''
def send_t10_html_mail(mail_title, template_str, mail_to, mail_from):
    mail_host = 'smtp.xxx.com.cn'  # 设置服务器
    mail_port = 465
    mail_user = 'xxx'  # 用户名
    mail_pass = 'xxx'  # 口令

    sender = mail_user
    receivers = mail_to
    message = MIMEText(template_str, 'html', 'utf-8')
    message['From'] = Header(mail_from, 'utf-8')  # 发送者
    message['To'] = Header(mail_to, 'utf-8')  # 接收者
    subject = '{}--{}'.format(mail_title, dateUtil.get_day(dateUtil.get_today(), -1))
    message['Subject'] = Header(subject, 'utf-8')

    try:
        # smtpObj = smtplib.SMTP()
        smtpObj = smtplib.SMTP_SSL(mail_host, mail_port)
        # smtpObj.connect(mail_host, mail_port)    # 25 为 SMTP 端口号
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")
