# -*- encoding: utf-8 -*-
'''
@文件        :sqlalchemy_util.py
@说明        : SQLAlchemy工具类
@时间        :2021-04-15 16:28:02
@作者        :wangxh
@版本        :1.0
'''

import os,sys,time
from common.base_logger import logger
from common import config_utils as config_util
from common.constants import root_parent_path
from common.constants import app_name
from common.constants import env
from sqlalchemy import Column, String, create_engine
#from sqlalchemy import Column, String, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sys.path.append(os.getcwd())
#print(sys.path)
from urllib.parse import quote_plus as urlquote


Base = declarative_base()

engine = None
session = None

def init_db(env=None):
    #db_url = config_utils.get_value("mysql_bi_wh_data", "db.url")
    #db_passw = 'f%%!kdku9fTn3r'
    #db_url = 'mysql+pymysql://root:{}@10.88.5.153:3306/bi-wh-data'.format(urlquote(db_passw))

    if env == None:
        env = 'test'
    db_passw = config_util.get_value('db.bi-wh-data.password', env)
    db_url = config_util.get_value('db.bi-wh-data.url', env).format(config_util.get_value('db.bi-wh-data.user_name', env),
                                                                    urlquote(db_passw), config_util.get_value('db.bi-wh-data.ip', env),
                                                                    config_util.get_value('db.bi-wh-data.port', env),
                                                                    config_util.get_value('db.bi-wh-data.db_name', env))

    engine = create_engine(db_url, encoding="utf-8", echo=True)
    print(engine)

'''
传入不同的key, 获取不同的数据源
'''
def get_db_session(env=None):
    #db_url = config_utils.get_value(key, "db.url")
    #db_passw = 'g91xlIa&YC0Y'
    #db_url = 'mysql+pymysql://root:{}@10.88.5.153:3306/bi-wh-data'.format(urlquote(db_passw))

    if env == None:
        env = 'test'
    db_passw = config_util.get_value('db.bi-wh-data.password', env)
    db_url = config_util.get_value('db.bi-wh-data.url', env).format(config_util.get_value('db.bi-wh-data.user_name', env),
                                                               urlquote(db_passw), config_util.get_value('db.bi-wh-data.ip', env),
                                                               config_util.get_value('db.bi-wh-data.port', env),
                                                               config_util.get_value('db.bi-wh-data.db_name', env))


    engine = create_engine(db_url, encoding="utf-8", echo=True)
    # 创建DBSession类型
    DBSession = sessionmaker(bind=engine, autoflush=False, expire_on_commit=False)
    # 创建session对象
    session = DBSession()

    return session

'''
db_name: .db文件路径
'''
def get_sqlite_db_session():

    db_name = os.path.join(root_parent_path, app_name,  'db', app_name + '-' + env + '.db')
    logger.info(f'db_name : {db_name}')
    db_url = "sqlite:///" + db_name
    engine = create_engine(db_url, encoding="utf-8", echo=True)
    # 创建DBSession类型
    DBSession = sessionmaker(bind=engine, autoflush=False, expire_on_commit=False)
    # 创建session对象
    session = DBSession()

    return session

def dictToObj(results, to_class):
    """将字典list或者字典转化为指定类的对象list或指定类的对象
    python 支持动态给对象添加属性，所以字典中存在而该类不存在的会直接添加到对应对象
    """
    if isinstance(results, list):
        objL = []
        for result in results:
            obj = to_class()
            for r in result.keys():
                obj.__setattr__(r, result[r])
            objL.append(obj)
        return objL
    elif isinstance(results, dict):
        obj = to_class()
        for r in results.keys():
            obj.__setattr__(r, results[r])
        return obj
    else:
        print("传入对象非字典或者list")
        return None


if __name__=="__main__":
    pass
    session = get_db_session('prod')
    sql = '''SELECT * FROM wh_ssc_middle_group_data where table_name='wh_distribution_sales_bill_detail' AND busi_date='2023-02-24' AND storeCode='096474002' AND detailBusiTypeCode='2520' ORDER BY create_time desc LIMIT 1;'''
    data_ist = session.execute(sql).fetchall()  # 执行后使用.fetchall()得到全部的语句
    #object_list = dictToObj(data_ist, wh_ssc_middle_group_data.WhSscMiddleGroupDatum)
    #print(object_list)

