# -*- encoding: utf-8 -*-
'''
@文件        :sqlite_util.py
@说明        :
@时间        :2021-12-02 15:41:15
@作者        :wangxh
@版本        :1.0
'''

from common.base_logger import logger


if __name__=="__main__":
    day = '2023-03-13'
    logger.info(f'Today is {day}')
