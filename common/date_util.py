
import os
import sys
import datetime,time,calendar
import random

def get_today():
    return datetime.date.today()

'''
获取昨天 明天等日期
'''
def get_day(num):
    today = datetime.date.today()
    day = today + datetime.timedelta(days=num)

    return day

def get_day(date, num):
    day = date + datetime.timedelta(days=num)
    return day

'''
日期时间格式化(yyyy-MM-dd HH:mm:ss)
'''
def format_day(date_time):
    if date_time == None:
        date_time = datetime.datetime.now()
    str_time = date_time.strftime("%Y-%m-%d %H:%M:%S")
    return str_time
    pass

'''
时间格式化(yyyy-MM-dd)
'''
def format_days(date_time):
    if date_time == None:
        date_time = datetime.datetime.now()
    str_time = date_time.strftime("%Y-%m-%d")
    return str_time



'''
时间格式化 精确到毫秒
'''
def format_now():
    str_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    return str_time

def gen_random_str():
    str = ""
    for i in range(8):
        ch = chr(random.randrange(ord('0'), ord('9') + 1))
        str += ch

    #print(str)
    return str


'''
日期时间格式化
'''
def format_day_time(date_time):
    if date_time == None:
        date_time = datetime.datetime.now()
    str_time = date_time.strftime("%Y-%m-%d %H:%M:%S")
    return str_time
    pass

def format_day_str(date_time):
    if date_time == None:
        date_time = datetime.datetime.now()
    str_time = date_time.strftime("%Y-%m-%d")
    return str_time
    pass

def get_first_and_last_day_of_month():
    now = datetime.datetime.now()
    year = now.year
    month = now.month
    last_day = calendar.monthrange(year, month)[1]
    start = datetime.date(year, month, 1).strftime('%Y-%m-%d')
    end = datetime.date(year, month, last_day).strftime('%Y-%m-%d')

    return start,end

def parse_day(date_str):
    if date_str == None or date_str == '':
        return None
    return datetime.datetime.strptime(date_str, '%Y-%m-%d')

def parse_day_time(date_time_str):
    if date_time_str == None or date_time_str == '':
        return None
    return datetime.datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S')



if __name__=="__main__":
    #print(get_today())
    #print(get_day(-1))
    #print(format_day_str(None))
    #print(get_first_and_last_day_of_month())

    gen_random_str()

    pass
