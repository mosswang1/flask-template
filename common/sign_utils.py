
import base64
import hashlib
import hmac

def get_hamc_sha1(message, key):
    message = message.encode(encoding="UTF-8")  # 加密内容
    key = key.encode(encoding="UTF-8")          # 加密的key
    result = hmac.new(key, message, hashlib.sha1).digest()  # 返回结果：b'\xd5*\x01\xb0\xa4,y\x96\x9d`\xd7\xfcB\xe1\x95OZIe\xe7'
    _sig = base64.b64encode(result).decode(encoding="UTF-8")
    return _sig


#