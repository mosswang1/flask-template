# coding: utf-8
from sqlalchemy import Column, Integer, Text, text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class SysConfig(Base):
    __tablename__ = 'sys_config'

    id = Column(Integer, primary_key=True, server_default=text("主键"))
    config_key = Column(Text(100), unique=True)
    config_value = Column(Text(255))
    config_status = Column(Text(1))
    remark = Column(Text(255))
    enable_time = Column(Text)
    delete_flag = Column(Text(1))
    create_by = Column(Text(50))
    create_time = Column(Text)
    update_by = Column(Text(50))
    update_time = Column(Text)
