# coding: utf-8
from sqlalchemy import Column, Date, String
from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class SmPullConf(Base):
    __tablename__ = 'sm_pull_conf'

    id = Column(INTEGER(11), primary_key=True, comment='主键')
    conf_type = Column(String(255), nullable=False, unique=True, comment='配置类型')
    last_pull_date = Column(Date, nullable=False, comment='最后拉取日期')
    pull_status = Column(TINYINT(255), nullable=False, comment='拉取状态（0：未拉取，1：拉取成功，2：拉取失败）')
    pull_hour = Column(String(10), comment='拉取开始的小时 00-23')
    pull_page_num = Column(INTEGER(10), comment='拉取开始的页码')
    table_name = Column(String(100), comment='表名')
    job_name = Column(String(255), comment='任务名称')
