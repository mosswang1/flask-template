# -*- encoding: utf-8 -*-
'''
@文件        :db_service.py
@说明        :
@时间        :2021-11-25 15:43:50
@作者        :wangxh
@版本        :1.0
'''

import os,sys,time,logging
logging.basicConfig(level=logging.INFO)
import json

from sqlalchemy import Column, String, create_engine
#from sqlalchemy import Column, String, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sys.path.append(os.getcwd())
#print(sys.path)
from db.enity import sm_pull_conf as SmPullConf
#import orm_util as orm_util
from common import sqlalchemy_util as SQLAlchemyUtil
from common import date_util as dateUtil
from common.constants import env


def save_bi_data_list(list):
    with SQLAlchemyUtil.get_db_session(env='prod') as session:
        # for item in list:
        #     if item.hotel_join_type == None or item.hotel_join_type == '':
        #         print(item)
        #session.add_all(list)
        session.bulk_save_objects(list)
        #session.bulk_insert_mappings(FinanceExcelRule, list)
        # session.execute(FinanceExcelRule.__tablename__.insert(), list)
        session.commit()
        session.close()


def get_bi_wh_pull_data():
    session = SQLAlchemyUtil.get_db_session("mysql_bi_wh_data")
    bi_wh_pull_conf_list = session.query(SmPullConf).all()
    #print(bi_wh_pull_conf.to_dict())

    # for item in bi_wh_pull_conf_list:
    #     print(item.to_dict())

    session.close()
    
    return bi_wh_pull_conf_list

# def get_joint_wisdom_data():
#     session = SQLAlchemyUtil.get_db_session("mysql_joint_wisdom_data")
#     joint_wisdom_list = session.query(joint_wisdom_pull_conf).all()
#
#     # for item in joint_wisdom_list:
#     #     print(item.to_dict())
#
#     session.close()
#
#     return joint_wisdom_list

'''
传入不同的job_group, 获取不同的日志ID
'''
# def get_xxl_jod_data(job_group):
#     # 获取前一天的日期 dateUtil.get_day(-1)
#     session = SQLAlchemyUtil.get_db_session("mysql_xxljob")
#     xxl_job_log_list = session.query(xxlJobLog).filter(xxlJobLog.job_group==job_group,
#         xxlJobLog.trigger_time.like('{}%'.format(dateUtil.get_day(-1)))).all()
#
#     # for item in xxl_job_log_list:
#     #     print(item.to_dict())
#
#     return xxl_job_log_list

if __name__=="__main__":
    #get_bi_wh_pull_data()
    #get_joint_wisdom_data()
    #get_xxl_jod_data(2)
    print(dateUtil.format_day(None))
    pass
