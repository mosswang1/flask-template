import json
import copy
from datetime import date, timedelta, datetime

from common.constants import env
from common import date_util
from common import bi_wh_data as py_mysql
from common import  sqlalchemy_util as SQLAlchemyUtil
from common.base_logger import logger as logging
from db.enity import sys_config

def get_all_lists():
    res_list = []
    with SQLAlchemyUtil.get_sqlite_db_session() as session:
        conf_list = session.query(sys_config.SysConfig).all()
        res_list = conf_list
        # res_list = [dict(zip(result.keys(), result)) for result in conf_list]
        # if res_list:
        #     for item in res_list:
        #         item['last_pull_date'] = date_util.format_day_str(item.get('last_pull_date'))


    return res_list