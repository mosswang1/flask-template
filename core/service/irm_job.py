import json
import copy
from datetime import date, timedelta, datetime

from common.constants import env
from common import date_util
from common import bi_wh_data as py_mysql
from common import  sqlalchemy_util as SQLAlchemyUtil
from common.base_logger import logger as logging
from db.enity import sm_pull_conf

def do_job():
    start_date = date_util.format_now()
    logging.info(f'irm job start: {start_date}')
    logging.info(f'irm job execute...')
    end_date = date_util.format_now()
    logging.info(f'irm job end: {end_date}')
