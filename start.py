import os
import sys

from common.base_logger import logger
from common.constants import env

from flask import Flask
from flask import Flask,Blueprint,render_template
from flask_apscheduler import APScheduler

from core.service import irm_job
from web.sys_config import sys_config_bp
from web.index import index_bp
from web.webhooks import webhooks_bp

app = Flask(__name__)

def creat_app():
    app.register_blueprint(sys_config_bp)
    app.register_blueprint(index_bp)
    app.register_blueprint(webhooks_bp)

    # 定时任务
    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.add_job(func=irm_job.do_job, id='irm_job', trigger='cron', hour='04', minute='10')
    scheduler.start()

    return app

@app.route('/')
def hello_world():
    return 'Hello World!'

if __name__ == '__main__':
    print(f'before set env: {env}')

    if len(sys.argv) > 1:
        #env = 'test'
        env = sys.argv[1]
        print(f'after set env: {env}')
        #irm_job.do_job('2023-03-01', '2023-03-31')
    else:
        env = 'dev'
        print(f'after set env: {env}')

    debug = True
    if env == 'prod':
        debug = False

    app = creat_app()
    # 启动一个flask web应用 http://127.0.0.1:5000
    app.run(host="0.0.0.0", port=int("5000"), debug=debug, use_reloader=False)





