
生成requirements.txt
```
# 安装pipreqs
pip install pipreqs

cd D:\python_workspace\wangxh-tools 

pipreqs ./ --encoding utf-8 --force

# 根据requirements.txt安装依赖
pip install -r requirements.txt
```

虚拟环境相关操作
```
# 激活虚拟环境
cd /opt/job-platform/executors/bi-data-executor/ext/wangxh-tools
source wangxh-tools-venv/bin/activate

# 退出虚拟环境
deactivate

```

启动命令
```
python start.py dev
```





