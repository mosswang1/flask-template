import os
import time


from common import logger
from common.constants import root_path
from common.constants import root_parent_path

def print_numbers():
    for i in range(1, 101):
        logger.info(f"i: {i}, i * 2: {i * 2}")
        time.sleep(1)


if __name__ == '__main__':
    print_numbers()
    #parent_dir = root_parent_path
    #print(parent_dir)