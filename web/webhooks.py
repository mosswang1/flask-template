import json
from flask import Blueprint, render_template, request

from common.base_logger import logger

webhooks_bp = Blueprint('webhooks_bp', __name__, url_prefix='/server/webhooks')



@webhooks_bp.post("gitlab")
def execute_gitlab_hooks():
    # X-Gitlab-Token
    acess_token = request.headers.get('X-Gitlab-Token')
    request_param = json.loads(request.data.decode())
    logger.info("access_token: {}".format(acess_token))
    logger.info(request_param)
    return "success"