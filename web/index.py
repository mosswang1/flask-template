from flask import Blueprint, render_template, request


index_bp = Blueprint('index_bp', __name__, url_prefix='/index')


cloud_list = {
    "clouds" : [
        {
            "name" : "AWS",
            "href" : "https://aws.amazon.com/"
        },
        {
            "name" : "GCP",
            "href" : "https://gcp.infoq.cn/index.html"
        },
        {
            "name" : "Azure",
            "href" : "https://azure.microsoft.com/"
        }
    ]
}

@index_bp.get("cloud/provider/list")
def get_sm_confs_list():

    return cloud_list