from flask import Blueprint, render_template, request

from core.service import sys_config_service

sys_config_bp = Blueprint('sys_config_bp', __name__, url_prefix='/sys/config')

@sys_config_bp.get("list")
def get_sm_confs_list():
    all_list = sys_config_service.get_all_lists()
    return render_template('sys_config_list.html', conf_list=all_list)

