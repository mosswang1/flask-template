FROM ubuntu:20.04
MAINTAINER wangxh
ENV TZ=Asia/Shanghai \
    DEBIAN_FRONTEND=noninteractive \
    APP_ENV=sit

WORKDIR /home/wangxh/flask-template
COPY requirements.txt /home/wangxh/

RUN sed -i s@/archive.ubuntu.com/@/mirrors.aliyun.com/@g /etc/apt/sources.list
RUN apt-get update \
	&& apt-get install -y vim \
	&& apt-get install -y tzdata \
	&& ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
    && echo ${TZ} > /etc/timezone \
    && dpkg-reconfigure --frontend noninteractive tzdata \
	&& apt-get install -y python3 \
	&& apt-get install -y python3-pip \
	&& pip3 install -i https://mirrors.aliyun.com/pypi/simple/ --upgrade pip \
	&& pip3 config set global.index-url https://mirrors.aliyun.com/pypi/simple/ \
	&& pip3 install -r /home/wangxh/requirements.txt \
	&& apt clean \
	&& rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp*

COPY . /home/wangxh/flask-template/

RUN chmod +x restart.sh

EXPOSE 5000

ENTRYPOINT [ "python3" ]
CMD [ "start.py" ]
#CMD ["/bin/bash","-D"]


